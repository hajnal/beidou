## 北斗 WebApp 端

### 技术栈
* vue + vue-router + axios + webpack + vant (UI库)

### 移动端适配

手淘适配方案 rem + px2rem，以375的设计稿为基准，直接写 px ，自动转成 rem 进行适配

*  `main.js` 引入 `import 'lib-flexible'`

*  配置 `postcssrc.js` 

这边vant需要，以37.5为基准，propList中!的属性不进行转换，如果个别样式中不需要转换的可以写Px

``` js
'postcss-pxtorem': {
      rootValue: 37.5,
      propList: ['*', '!font-size', '!border', '!border-width']
    }
```
*  `build/utils.js` 配置 `px2rem` 的 `postcss` 插件

``` js
 function generateLoaders (loader, loaderOptions) {
    const loaders = options.usePostCSS ? [cssLoader, postcssLoader] : [cssLoader]

```


### 启动

``` bash
# 下载
git clone https://gitlab.com/hajnal/beidou.git

# 安装依赖
npm install

# 启动服务
npm run dev

# 打包
npm run build
```

### 本地调试

* `dev` 下 `config/index.js` 开启 webpack 代理，解决跨域问题

``` js
proxyTable: {
      '/apis': {
        // 测试环境
        target: 'http://beidou.edianlai.com',  // 接口域名
        changeOrigin: true,  //是否跨域
        pathRewrite: {
          '^/apis': ''   //需要rewrite重写的,
        }              
      }
    },
```


* `config/index.js` 可配置启动的 ip 和 端口 
* 手机端查看，需要同 wifi 下，然后把 host 改为电脑的ip

``` js
host: 'localhost',
port: 8081
```

### 文件说明

* `src/request.js` 配置axios，请求结果预处理、拦截器等

``` js
// 打包和开发模式下，需要切换 baseURL，将其一注视即可

// dev
axios.defaults.baseURL = 'apis/beidou/api'

// product
axios.defaults.baseURL = '/beidou/api'
```

* `utils/api.js` 统一管理接口名
* `utils/mixin.js` 各.vue文件的公用方法，暂时只有logout
* `router/index.js` 配置路由

### Todo

后台接口未完善

*  分类页 `/shop` 和商品页 `/good` 的批量删除 `delAll()`
*  商品编辑新增页 `/goodDetail` 的图片上传
*  `/web` 手机扫码，二维码图片替换



