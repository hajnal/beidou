import Vue from 'vue'
import Router from 'vue-router'

// 路由懒加载
const Web = () => import(/* webpackChunkName: "web" */'@/pages/web')
const Login = () => import(/* webpackChunkName: "login" */'@/pages/login')
const Register = () => import(/* webpackChunkName: "register" */'@/pages/register')
const Home = () => import(/* webpackChunkName: "home" */ '@/pages/home')
const OpenShop = () => import(/* webpackChunkName: "openShop" */'@/pages/openShop')
const Shop = () => import(/* webpackChunkName: "shop" */'@/pages/shop')
const Good = () => import(/* webpackChunkName: "good" */'@/pages/good')
const GoodDetail = () => import(/* webpackChunkName: "goodDetail" */ '@/pages/goodDetail')

Vue.use(Router)

const isMobile = /Mobile/i.test(navigator.userAgent)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Root',
      component: Login
    },
    // pc端扫码页
    {
      path: '/web',
      name: 'Web',
      component: Web
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    // 店铺列表页
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: { requireAuth: true }
    },
    // 分类列表页
    {
      path: '/shop',
      name: 'Shop',
      component: Shop,
      meta: { requireAuth: true }
    },
    // 开店页
    {
      path: '/openShop',
      name: 'OpenShop',
      component: OpenShop,
      meta: { requireAuth: true }
    },
    // 商品列表页
    {
      path: '/good',
      name: 'Good',
      component: Good,
      meta: { requireAuth: true }
    },
    // 商品修改、新增页
    {
      path: '/goodDetail',
      name: 'GoodDetail',
      component: GoodDetail,
      meta: { requireAuth: true }
    }
  ]
})

// 路由拦截器
router.beforeEach((to, from, next) => {
  // 电脑端拦截
  if (to.name !== 'Web' && !isMobile) {
    next({
      path: '/web'
    })
  }
  // 手机端拦截
  if (to.name === 'Web' && isMobile) {
    next({
      path: '/login'
    })
  }
  // 登录拦截
  if (to.matched.some(res => res.meta.requireAuth)) {
    if (localStorage.getItem('user')) {
      next()
    } else {
      next({
        path: '/login'
      })
    }
  } else {
    next()
  }
})

export default router
