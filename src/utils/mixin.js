import { API } from '@/utils/api'
export default {
  methods: {

    // 退出登录
    logout () {
      this.axios.post(API.logout, {}).then(data => {
        localStorage.clear()
        this.$router.push({ name: 'Login' })
      })
    }
  }
}
