
const API = {
  login: '/webapp_login',
  register: '/web_user_reg',
  getCode: '/code_get',
  logout: '/webapp_logout',
  editShop: '/web_shop_name',
  openShop: '/web_open_shop',
  closeShop: '/web_close_shop',
  getCategories: '/web_get_category',
  modifyCategory: '/web_set_category',
  addCategory: '/web_set_category',
  getGoods: '/web_get_goods',
  modifyGood: '/web_modify_good',
  addGood: '/web_add_good'
}

export { API }
